from PyQt5.QtCore import QSize, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QLineEdit, QHBoxLayout, QWidget, QPushButton,QLineEdit
from pytube import *

import sys


class main(QMainWindow):
    def __init__(self):
        super().__init__()
        layout = QHBoxLayout()

        self.setWindowTitle("Youtube_downloader")
        button1 = QPushButton("enter")
        self.label = QLineEdit()
        button1.setCheckable(True)
        button1.clicked.connect(self.download)
        layout.addWidget(button1)
        layout.addWidget(self.label)
        container = QWidget()
        container.setLayout(layout)
        self.setCentralWidget(container)
    def download(self, checked):
        url = self.label.text()
        YouTube(url).streams.first().download()


app = QApplication(sys.argv)

window = main()
window.show()

app.exec()

